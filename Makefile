MathMethods.pdf: MathMethods-paper.bib MathMethods-paper.Rnw MathMethods-script.R brownian-functions.R
	#Clean up, so we can start fresh.
	rm -f *.aux *.log *.pdf *.upa *.upb *synctex* *concordance* *.bbl *.blg *.out #remove LaTeX auxilarry files
	rm -rf plots cache #Remove folders created by knitr
	rm -f *.tex #remove .tex file from last time.
	#Close other preview windows
	osascript -e 'quit app "preview"'
	#actually compile the file
	R -e 'library(knitr);knit("MathMethods-paper.Rnw")' #Run Knitr in R
	pdflatex MathMethods-paper #Run through the 4 LaTeX cycle
	bibtex MathMethods-paper
	pdflatex MathMethods-paper
	pdflatex MathMethods-paper
	open MathMethods-paper.pdf #Let's take a look.
